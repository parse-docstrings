;;;; -*- Mode: lisp; indent-tabs-mode: nil -*-

(asdf:defsystem parse-docstrings
  :depends-on (:closer-mop :iterate :hyperdoc :split-sequence)
  :serial t
  :components ((:file "package")
               (:file "annotate-documentation")
               (:file "classes")
               (:file "annotation-plist")
               (:file "parse-docstrings")
               (:file "sexp")
               (:file "syntax-plugins/syntax-sbcl")
               (:file "syntax-plugins/syntax-atsign")))

;; vim: ft=lisp et
