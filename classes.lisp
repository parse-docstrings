;;; -*- lisp; show-trailing-whitespace: t; indent-tabs: nil -*-

;;;; Part of this software was originally written as docstrings.lisp in
;;;; SBCL, but is now part of the parse-docstrings project.  The file
;;;; docstrings.lisp was written by Rudi Schlatte <rudi@constantly.at>,
;;;; mangled by Nikodemus Siivola, turned into a stand-alone project by
;;;; Luis Oliveira.  SBCL is in the public domain and is provided with
;;;; absolutely no warranty.

;;;; parse-docstrings is:
;;;;
;;;; Copyright (c) 2008 David Lichteblau:
;;;;
;;;; Permission is hereby granted, free of charge, to any person
;;;; obtaining a copy of this software and associated documentation
;;;; files (the "Software"), to deal in the Software without
;;;; restriction, including without limitation the rights to use, copy,
;;;; modify, merge, publish, distribute, sublicense, and/or sell copies
;;;; of the Software, and to permit persons to whom the Software is
;;;; furnished to do so, subject to the following conditions:
;;;;
;;;; The above copyright notice and this permission notice shall be
;;;; included in all copies or substantial portions of the Software.
;;;;
;;;; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;; EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;; MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
;;;; NONINFRINGEMENT.  IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
;;;; HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
;;;; WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
;;;; OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
;;;; DEALINGS IN THE SOFTWARE.

#+sbcl ;; FIXME: should handle this in the .asd file
(eval-when (:compile-toplevel :load-toplevel :execute)
  (require 'sb-introspect))

(in-package #:parse-docstrings)


;;; The DOCUMENTATION* class

(defvar *documentation-package*)
(defvar *documentation-package-name*)

(defclass documentation* (parent-mixin)
  ((name :initarg :name
	 :reader get-name)
   (kind :initarg :kind
	 :reader get-kind)
   (package :initform *documentation-package*
	    :reader get-package)
   (package-name :initform *documentation-package-name*
                 :reader get-package-name)
   (annotations :initform nil
		:accessor documentation-annotations)
   (methods :initform nil
	    :initarg :methods
	    :accessor method-documentation-objects)))


;;;; Markup classes

(defclass markup-element ()
  ()
  (:documentation
   "MARKUP-ELEMENT is the superclass of all markup elements in
    parse-docstrings.

    Wikipedia says: `A markup language is an artificial language using a set
    of annotations to text that give instructions regarding the structure of
    text or how it is to be displayed.'

    Documentation, usually stored in .lisp files in the form of docstrings
    and their annotations, is parsed into a structured CLOS representation by
    parse-docstrings.  At the highest level, instances of BLOCK-CONTENT as
    used, for example PARAGRAPH object, CODE-BLOCK objects, or ITEMIZATION.
    At lower levels, elements like BOLD or HYPERLINK provide structure
    within paragraphs."))

(annotate-documentation (markup-element type)
  (:see-also-type documentation*)
  (:see-also-function documentation-content)
  (:see-also-type documentation-annotations)
  (:see-also-type parent-mixin)
  (:see-also-function markup-to-sexp)
  (:see-also-function sexp-to-markup))

(defclass block-content (markup-element)
  ()
  (:documentation
   "The common superclass of all markup elements that can appear directly
    as children of a DOCUMENTATION* object, for example PARAGRAPH objects.

    The distinction between BLOCK-CONTENT and INLINE-CONTENT is not being
    enforced strictly by parse-docstring functions currently.  However, output
    generation will to many formats is expected to be easier if syntax
    plugins adhere to the convention that inline elements don't appear at
    the top themselves and don't have block elements as children."))

(annotate-documentation (block-content type)
  (:see-also-type inline-content))

(defclass inline-content (markup-element)
  ()
  (:documentation
   "The common superclass of all markup elements that can appear at a lower
    level in the markup tree, e.g. as a part of paragraphs.

    The distinction between BLOCK-CONTENT and INLINE-CONTENT is not being
    enforced strictly by parse-docstring functions currently.  However, output
    generation will to many formats is expected to be easier if syntax
    plugins adhere to the convention that inline elements don't appear at
    the top themselves and don't have block elements as children."))

(annotate-documentation (inline-content type)
  (:see-also-type block-content))

(defclass parent-mixin ()
  ((child-elements :initarg :child-elements
		   :accessor child-elements))
  (:documentation
   "The mixin superclass of all markup elements that have child elements.
    This mixin is used by almost every markup class, except those that are
    actual leafs (like TEXT) and those that have non-markup child objects
    interposed before the next level of markup (like DEFINITION-LIST
    with its DEFINITION-LIST-ITEM objects as non-element children).")
  (:default-initargs :child-elements nil))

(annotate-documentation (parent-mixin type)
  (:slot-accessor child-elements)
  (:see-also-type markup-element))

(annotate-documentation (child-elements function)
  (:argument object "An instance of PARENT-MIXIN")
  (:return-value "A list of MARKUP-ELEMENT instances.")
  "Returns the children of this parent.

   For elements of type BLOCK-CONTENT, the children are expected
   to be of type BLOCK-CONTENT or INLINE-CONTENT, while element of type
   INLINE-CONTENT are expected to have only INLINE-CONTENT children.
   This distinction is not being enforced by parse-docstrings at this
   point, however.")

(defmethod print-object ((object parent-mixin) stream)
  (print-unreadable-object (object stream :type t)
    (prin1 (child-elements object) stream)))

(defclass text (inline-content)
  ((characters :initarg :characters
	       :accessor characters))
  (:documentation
   "Instances of this class wrap all strings that constitute the actual
    text of a documentation string."))

(annotate-documentation (text type)
  (:slot-accessor characters)
  (:constructor make-text))

(annotate-documentation (characters function)
  (:argument object "An instance of TEXT")
  (:return-value "STRING")
  "Returns the contents of this TEXT node as string.

   Since TEXT nodes represent parsed markup, there are no special
   characters in this text.  

   However, it is strongly recommended that ASCII control character not
   be used in the string, except for whitespace characters, because such
   characters cannot be represented at all in XML and HTML output formats.

   Normally, newlines and other kinds of whitespace behave like an ordinary
   space character, and sequences of whitespace are condensed into a single
   space.  However, some parent elements will specify different rules.
   In particular, the PREFORMATTED and CODE-BLOCK element will render whitespace
   exactly as contained in the original string.")

(defclass preformatted (parent-mixin block-content)
  ()
  (:documentation
   "Instances of this class define an exception from the usual formatting
    of TEXT nodes for their children (and ancestor elements).

    Here, newlines are preserved as line breaks, and sequences of space
    are not condensed into a single space.

    This element is similar to HTML's pre element, or the CSS assignment
    `white-space: pre'.

    When rendered to HTML, output formats may want to use different CSS
    classes for PREFORMATTED and CODE-BLOCK, so that users can customize appearance
    easily in a CSS file."))

(annotate-documentation (preformatted type)
  (:constructor make-preformatted))

(defclass code-block (preformatted)
  ()
  (:documentation
   "A subclass of PREFORMATTED, this element can be used for code written
    in a programming language.

    Output formats might render CODE-BLOCK elements in a slightly different
    style than ordinary PREFORMATTED elements, but are not required to do
    so.

    When rendered to HTML, output formats may want to use different CSS
    classes for PREFORMATTED and CODE-BLOCK, so that users can customize appearance
    easily in a CSS file. 

    Currently, the programming language used is not presented in markup.
    (Future versions of parse-docstrings might provide such an annotation,
    to allow language-specific syntax highlighting.)"))

(annotate-documentation (code-block type)
  (:constructor make-code-block))

;; these aren't parents, because their children aren't markup
(defclass list-mixin ()
  ((items :initarg :items
	  :accessor list-items))
  (:documentation
   "The mixin is a superclass of markup elements with a list of `children'
    that are not proper markup elements, because they cannot appear as
    child elements of other nodes."))

(annotate-documentation (list-mixin type)
  (:slot-accessor list-items)
  (:see-also-type list-element)
  (:see-also-type markup-element))

(annotate-documentation (list-items function)
  (:argument object "An instance of LIST-MIXIN")
  (:return-value "A list of LIST-ITEM instances.")
  "Returns the items of this list.")

(defclass definition-list (list-mixin block-content)
  ()
  (:documentation
   "Instances of this class represent lists of markup elements that is
    rendered with special heading for each element.

    Only objects of type DEFINITION-LIST-ITEM can occur as items
    in this list.

    This class is similar to DL element in HTML, with its LIST-ITEM children
    corresponding to DT and DL elements."))

;; theres aren't markup by themselves, because they can only appear in a
;; list, but their children are markup again
(defclass list-item (parent-mixin)
  ()
  (:documentation
   "Instances of this class represent items in an object of type LIST-MIXIN.

    Items have children of type MARKUP-ELEMENT, but are not markup themselves,
    because they can only appear as children of LIST-MIXINs."))

(defclass definition-list-item (list-item)
  ((title :initarg :title
	  :accessor definition-title))
  (:documentation
   "Instances of this class represent items in an object of type
    DEFINITION-LIST.

    Each item has a title and children, which are rendered next to each
    other at the item's position in the list.

    This class is similar to the DT and DL elements in HTML, which appear
    as children of a DL list."))

(annotate-documentation (definition-list-item type)
  (:slot-accessor definition-title)
  (:constructor make-definition-list-item))

(annotate-documentation (definition-title function)
  (:argument object "An instance of DEFINITION-LIST-ITEM")
  (:return-value "STRING")
  "Returns the title that is rendered as a header for this item.")

(defclass itemization (list-mixin block-content)
  ()
  (:documentation
   "Instances of this class represent lists of markup elements that will
    be rendered similarly to paragraphs, but usually with indentation and
    bullet points.

    This class is similar to UL element in HTML, with its LIST-ITEM children
    corresponding to LI elements."))

(annotate-documentation (itemization type)
  (:constructor make-itemization))

(defclass enumeration (list-mixin block-content)
  ()
  (:documentation
   "Instances of this class represent lists of markup elements that will
    be rendered similarly to paragraphs, but usually with indentation and
    a sequential numbering starting with 1.

    This class is similar to OL element in HTML, with its LIST-ITEM children
    corresponding to LI elements."))

(annotate-documentation (enumeration type)
  (:constructor make-enumeration))

(defclass paragraph (parent-mixin block-content)
  ()
  (:documentation
   "Instances of this class represent blocks of content beginning on a
    fresh line and ending with a line break.

    In contrast to DIV, the first line should have a margin of white space at
    the top,and the last a margin of white space at the bottom.

    This class is similar to the P element in HTML."))

(defclass div (parent-mixin block-content)
  ()
  (:documentation
   "Instances of this class represent blocks of content beginning on a
    fresh line and ending with a line break.

    In contrast to PARAGRAPH, there should be no margin of white space at
    the top or bottom for the content of this element.

    This class is similar to the DIV element in HTML."))

(defclass span (parent-mixin inline-content)
  ()
  (:documentation
   "Instances of this class represent a sequence of inline markup that is
    rendered together, in the specified order.

    This class is similar to the SPAN element in HTML."))

(defclass bold (parent-mixin inline-content)
  ()
  (:documentation
   "Instances of this class render their text in a bold font.

    This class is similar to the B element in HTML, or the font-weight: bold
    assignment in CSS."))

(defclass italic (parent-mixin inline-content)
  ()
  (:documentation
   "Instances of this class render their text in an italic font.

    This class is similar to the I element in HTML, or the font-style: italic
    assignment in CSS."))

(defclass fixed-width (parent-mixin inline-content)
  ()
  (:documentation
   "Instances of this class render their text in a fixed-width font.

    This class is similar to the TT element in HTML, or the
    font-family: monospace assignment in CSS."))

(defclass inline-code (parent-mixin inline-content)
  ()
  (:documentation
   "Similar to FIXED-WIDTH, this element can be used for code written
    in a programming language.

    When rendered to HTML, output formats may want to use different CSS
    classes for FIXED-WIDTH and INLINE-CODE, so that users can customize
    appearance easily in a CSS file. 

    Currently, the programming language used is not presented in markup.
    (Future versions of parse-docstrings might provide such an annotation,
    to allow language-specific syntax highlighting."))

(defclass underline (parent-mixin inline-content)
  ()
  (:documentation
   "Instances of this class render their text underlined.

    This class is similar to the U element in HTML, or the text-decoration:
    underline assignment in CSS."))

(defclass hyperlink (parent-mixin inline-content)
  ((href :initarg :href
	 :accessor href))
  (:documentation
   "Instances of this class represent Hyperlinks.  Their href attribute
    specifies an URL as a string.

    If possible,output formats should render the children of the element,
    and use the href attribute only as the action when the element is
    selected -- just like the A element behaves in HTML.

    As an exception, output formats without native support for links, such
    as plaintext formatting, are encourages to display the URL verbatim,
    for example as a footnote or just following the child elements, using
    using brackets to distinguish the URL from surrounding text.

    If at all possible, do not use HYPERLINK elements to link to documentation
    of individual Lisp functions.  Instead, use cross references, either
    in the form of an INLINE-CROSS-REFERENCE or as a
    CROSS-REFERENCE-ANNOTATION, and use hyperdoc to define URLs for
    functions in the target package.

    Used this way, internal cross references to functions documented
    in the same run of parse-docstrings can use links native to the output
    format (for example, using relative URLs in #anchor format instead of
    absolute URLs) and will use the absolute URLs for external
    references only, with the URL format currently declared to be correct
    by the library being referenced."))

(annotate-documentation (href function)
  (:argument object "An instance of HYPERLINK")
  (:return-value "STRING")
  "Returns the title that is rendered as a header for this item.")

(defclass cross-reference-mixin ()
  ((target :initarg :target
	   :accessor cross-reference-target)
   (doc-type :initarg :doc-type
	     :accessor cross-reference-doc-type))
  (:documentation
   "Instances of this class specify cross references between documentation
    strings.  The target of a cross-reference is specified as a pair
    of a documentation name (for example, a symbol) and a doc-type, which
    would be suitable as an argument to DOCUMENTATION or DOCUMENTATION*."))

(annotate-documentation (cross-reference-mixin type)
  (:slot-accessor cross-reference-target)
  (:slot-accessor cross-reference-doc-type))

(defclass inline-cross-reference
    (parent-mixin
     cross-reference-mixin
     inline-content)
  ((annotation-category :initarg :annotation-category
			:accessor annotation-category))
  (:documentation
   "Instances of this class represent cross references (or links) to
    Lisp documentation.

    (If at all possible, use this class or CROSS-REFERENCE-ANNOTATION over
    HYPERLINK whenever possible.)

    The ANNOTATION-CATEGORY slot in this class can specify what kind
    of conceptual relationship the reference's target has to the documentation
    it is being used in.  Depending on the category, it will be listed
    as a slot accessor, constructor function, condition type signalled,
    or just listed under `see also'.  Refer to ANNOTATION-CATEGORY for
    permissible values.

    This class will render internal cross references to functions documented
    in the same run of parse-docstrings can use links native to the output
    format (for example, using relative URLs in #anchor format instead of
    absolute URLs) and will use the absolute URLs for external
    references only, with the URL format currently declared to be correct
    by the library being referenced."))

(annotate-documentation (inline-cross-reference type)
  (:slot-accessor annotation-category)
  (:constructor make-inline-cross-reference))

(annotate-documentation (annotation-category function)
  (:argument object "An instance of INLINE-CROSS-REFERENCE")
  (:return-value "One of :CONDITION, :CONSTRUCTOR, :SLOT-ACCESSOR, or NIL")
  "Returns the ANNOTATION-CATEGORY of the cross reference.

   The category specifies what kind of conceptual relationship the
   reference's target has to the documentation it is being used in.

   Possible values are:

     * NIL -- will be listed as `See Also'
     * :SLOT-ACCESSOR -- will be listed as `Slot Accessor Function'
     * :CONSTRUCTOR -- will be listed as `Constructor Function'
     * :CONDITION -- will be listed as `Condition Types Signalled'")

(defclass unknown-element (parent-mixin inline-content)
  ((name :initarg :name
	 :accessor name)
   (plist :initarg :plist
	  :accessor plist))
  (:documentation
   "This fallback class is used for unknown markup elements encountered
    in a docstring.

    A property list mapping strings (or symbols) to strings is provided
    as PLIST.

    Users are advised against use of unknown elements, because not all output
    formats will render them identically.

    Examples:

    * An XML serialization might pass unknown elements through under the
      specified name, perhaps in a special namespace.
    * An HTML serialization might pass those elements through that happen
      to have a name permitted in HTML, and ignore others.  Alternatively, it
      might allow all unknown elements, leaving it up to the docstring author
      to ensure that no invalid HTML will result.
    * Other output formats might ignore unknown elements completely, or
      render only their children.

    All output formats are encouraged (but not required to) continue
    execution without errors when encountering unknown elements, but also
    to print a warning alterting the user to their presence."))

(annotate-documentation (unknown-element type)
  (:slot-accessor name)
  (:slot-accessor plist)
  (:constructor make-unknown-element))

(annotate-documentation (name function)
  (:argument object "An instance of UNKNOWN-ELEMENT")
  (:return-value "STRING")
  "Returns the name of the unknown element.

   For example, an atdoc tag @foo[bar]{baz} would result in an
   UNKNOWN-ELEMENT with a NAME of \"foo\".")

(annotate-documentation (plist function)
  (:argument object "An instance of UNKNOWN-ELEMENT")
  (:return-value "A property list mapping strings or symbols to strings")
  "Returns the plist of the unknown element.

   For example, an atdoc tag @foo[bar]{baz} would result in an
   UNKNOWN-ELEMENT with a PLIST mapping \"foo\" to \"bar\".")

(defun make-inline-cross-reference
    (target doc-type annotation-category &rest children)
  (make-instance 'inline-cross-reference
		 :target target
		 :doc-type doc-type
		 :annotation-category annotation-category
		 :child-elements children))

(defun make-text (characters)
  (check-type characters string)
  (make-instance 'text :characters characters))

(macrolet ((%simple-parent (constructor class)
	     `(defun ,constructor (&rest children)
		(dolist (child children)
		  (check-type child markup-element))
		(make-instance ',class :child-elements children))))
  (%simple-parent make-preformatted preformatted)
  (%simple-parent make-code-block code-block)
  (%simple-parent make-paragraph paragraph)
  (%simple-parent make-div div)
  (%simple-parent make-span span)
  (%simple-parent make-bold bold)
  (%simple-parent make-italic italic)
  (%simple-parent make-fixed-width fixed-width)
  (%simple-parent make-inline-code inline-code)
  (%simple-parent make-underline underline))

(macrolet ((%list-parent (constructor class)
	     `(defun ,constructor (&rest items)
		(dolist (item items)
		  (check-type item list-item))
		(make-instance ',class :items items))))
  (%list-parent make-itemization itemization)
  (%list-parent make-enumeration enumeration))

(defun make-definition-list (&rest items)
  (dolist (item items)
    (check-type item definition-list-item))
  (make-instance 'definition-list :items items))

(defun make-definition-list-item (title &rest children)
  (check-type title string)
  (dolist (child children)
    (check-type child markup-element))
  (make-instance 'definition-list-item :title title :child-elements children))

(defun make-list-item (&rest children)
  (dolist (child children)
    (check-type child markup-element))
  (make-instance 'list-item :child-elements children))

(defun make-hyperlink (href &rest children)
  (check-type href string)
  (dolist (child children)
    (check-type child markup-element))
  (make-instance 'hyperlink :href href :child-elements children))

(defun make-unknown-element (name plist &rest children)
  (check-type name string)
  (dolist (child children)
    (check-type child markup-element))
  (iter (for (name value) on plist by #'cddr)
	(check-type name (or symbol string))
	(check-type value string))
  (make-instance 'unknown-element :name name
		 :plist plist
		 :child-elements children))


;;;; Annotation Classes

(defclass documentation-annotations ()
  ((arguments :initform nil
	      :accessor argument-annotations)
   (return-values :initform nil
		  :accessor return-value-annotations)
   (conditions :initform nil
	       :accessor condition-annotations)
   (slot-accessors :initform nil
		   :accessor slot-accessor-annotations)
   (constructors :initform nil
		 :accessor constructor-annotations)
   (see-also-list :initform nil
		  :accessor see-also-annotations)
   ;; FIXME: shouldn't the following slots are contain lists, so that
   ;; multiple annotations can be merged properly?
   (summary :initform nil
	    :accessor summary-annotation)
   (implementation-note :initform nil
			:accessor implementation-note-annotation))
  (:documentation
   "This class stores annotations for docstrings, specifing for additional
    information on arguments, return values, and cross references.

    Depending on the docstring syntax in use, annotation can be specified
    directly in the docstring using special markup.

    Alternatively, the plist of symbol that names a definition can be used
    to store annotations separately from the docstring.  Refer to the
    ANNOTATE-DOCUMENTATION macro for details."))

(annotate-documentation (documentation-annotations type)
  (:slot-accessor argument-annotations)
  (:slot-accessor return-value-annotations)
  (:slot-accessor condition-annotations)
  (:slot-accessor slot-accessor-annotations)
  (:slot-accessor constructor-annotation)
  (:slot-accessor see-also-annotations)
  (:slot-accessor summary-annotation)
  (:slot-accessor implementation-note-annotation)
  (:constructor annotations))

(annotate-documentation (argument-annotations function)
  (:argument object "An instance of DOCUMENTATION-ANNOTATIONS")
  (:return-value "A list of PARAMETER-LIKE-ANNOTATION objects")
  "Returns annotations for this function's arguments.

   Each annotation records the name of the argument, and a docstring
   describing details of the argument, e.g. its type.

   It is recommended (but currently not required) that arguments
   in list correspond to the original names in the lambda list, and that
   their order is preserved.")

(annotate-documentation (return-value-annotations function)
  (:argument object "An instance of DOCUMENTATION-ANNOTATIONS")
  (:return-value "A list of PARAMETER-LIKE-ANNOTATION objects")
  "Returns annotations for this function's return values.

   Each annotation records a docstring describing details of return value,
   e.g. its type.  Additionally, a name can be specified for annotation,
   allowing HyperSpec-like descriptions of each return value.

   Only functions using multiple values will have more than one annotation
   in this slot.

   Where multiple values are used, annotations in this list are stored in the
   order of their return values.")

(annotate-documentation (condition-annotations function)
  (:argument object "An instance of DOCUMENTATION-ANNOTATIONS")
  (:return-value "A list of CROSS-REFERENCE-ANNOTATION objects")
  "Returns annotations for condition classes signalled by this function.

   Each entry is this list is a cross reference for a type, referring to
   a condition class that might be signalled.

   Entries in this list can occur in any order.")

(annotate-documentation (constructor-annotations function)
  (:argument object "An instance of DOCUMENTATION-ANNOTATIONS")
  (:return-value "A list of CROSS-REFERENCE-ANNOTATION objects")
  "Returns annotations for functions serving as constructors for this type.

   In this documentation, constructor for a type is any function that
   returns fresh instances of that type.

   This kind of annotation is useful when a type FOO is usually created
   through a wrapper function MAKE-FOO rather than direct calls to
   MAKE-INSTANCE.

   Entries in this list can occur in any order.")

(annotate-documentation (slot-accessor-annotations function)
  (:argument object "An instance of DOCUMENTATION-ANNOTATIONS")
  (:return-value "A list of CROSS-REFERENCE-ANNOTATION objects")
  "Returns annotations for functions serving as slot readers or accessors.

   Annotations in this list document that this class has a slot accessible
   through the function designated by the cross reference, and possibly
   settable through a setf function for the same name.

   This kind of annotation is most useful for programs that opt not to
   document slots directly, and prefer to document the slot accessors as
   ordinary functions instead.

   Entries in this list can occur in any order.")

(annotate-documentation (see-also-annotations function)
  (:argument object "An instance of DOCUMENTATION-ANNOTATIONS")
  (:return-value "A list of CROSS-REFERENCE-ANNOTATION objects")
  "Returns annotations for related definitions.

   Cross-reference annotations in this list are meant to augment the
   CONDITION-ANNOTATIONS, CONSTRUCTOR-ANNOTATIONS, and
   SLOT-ACCESSOR-ANNOTATIONS.  Any cross reference can be added to this list,
   assuming that the target of the cross reference is related to the current
   function, and that relation is not made explict in the docstring.

   Entries in this list can occur in any order.")

(annotate-documentation (summary-annotation function)
  (:argument object "An instance of DOCUMENTATION-ANNOTATIONS")
  (:return-value "NIL or a MARKUP-ELEMENT")
  "Returns markup with a short summary of the documentation. 

   This markup node may contain a short description of the object being
   documented, suitable for an abbreviated display in a table of contents
   or similar.

   If not set explicitly, it is customary to use the first line or first
   paragraph of the documentation in its place.")

(annotate-documentation (implementation-note-annotation function)
  (:argument object "An instance of DOCUMENTATION-ANNOTATIONS")
  (:return-value "NIL or a MARKUP-ELEMENT")
  "Returns markup with implementation notes.

   This markup node may contain notes relating to the implementation of
   documentation.")

(defclass cross-reference-annotation (cross-reference-mixin)
  ()
  (:documentation
   "Instances of this class specify cross references that are
    included in documentation only as annotations, i.e. not as a part
    of the main docstring's text.

    Cross reference annotations are recorded as a part of
    DOCUMENTATION-ANNOTATIONS objects, as CONDITION-ANNOTATIONS,
    SLOT-ACCESSOR-ANNOTATIONS, CONSTRUCTOR-ANNOTATIONS, or
    SEE-ALSO-ANNOTATIONS."))

(annotate-documentation (cross-reference-annotation type)
  (:constructor make-cross-reference-annotation))

(defun make-cross-reference-annotation (target doc-type)
  "Returns a fresh instance of CROSS-REFERENCE-ANNOTATION with the
   slots for CROSS-REFERENCE-TARGET and CROSS-REFERENCE-DOC-TYPE bound
   to TARGET and DOC-TYPE, respectively."
  (make-instance 'cross-reference-annotation
		 :target target
		 :doc-type doc-type))

(annotate-documentation (make-cross-reference-annotation function)
  (:argument target "A documentation name, for example a symbol.")
  (:argument doc-type "A documentation type as accepted by DOCUMENTATION*"
	     "as a second argument.")
  (:return-value "An instance of cross-reference-annotation"))

(defclass parameter-annotation (parent-mixin)
  ((name :initarg :name
	 :accessor parameter-name))
  (:documentation
   "Instances of this class specify documentation on function arguments and
    return values.

    The annotation specifies a name, which is required or at least highly
    recommended for arguments, and optional for return values.

    Parameter annotations are a markup parent.  Their child elements
    are  meant to document the argument or return value and
    should usually specify at least its type, but ideally also describe
    its purpose."))

(annotate-documentation (parameter-annotation type)
  (:slot-accessor parameter-name)
  (:constructor make-parameter-annotation))

(defun make-parameter-annotation (name &rest children)
  "Returns a fresh instance of PARAMETER-ANNOTATION with the slot for
   PARAMETER-NAME bound to name and CHILD-ELEMENTS bound to CHILDREN."
  (make-instance 'parameter-annotation
		 :name name
		 :child-elements children))

(annotate-documentation (make-parameter-annotation function)
  (:argument name "Symbol naming the argument or return value.")
  (:argument children "List of MARKUP-ELEMENT objects")
  (:return-value "A fresh PARAMETER-ANNOTATION."))


;;; allow cross references to symbols in packages that aren't currently
;;; loaded.  (At least in theory, hyperdoc can resolve these using
;;; its pre-generated index.)

(defclass missing-symbol ()
  ((symbol-name :initarg :symbol-name
		:accessor missing-symbol-name)
   (package-name :initarg :package-name
		 :accessor missing-symbol-package-name)))

(defmethod missing-symbol-name ((symbol symbol))
  (symbol-name symbol))

(defmethod missing-symbol-package-name ((symbol symbol))
  (package-name (symbol-package symbol)))

(defun make-missing-symbol (name package)
  (make-instance 'missing-symbol
		 :symbol-name name
		 :package (etypecase package
			    (string package)
			    (package (package-name package)))))
