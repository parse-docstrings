;;; Written by David Lichteblau.  Released into the public domain;
;;; feel free to copy&paste this file into your own software.

(in-package :parse-docstrings)		;change this line when copy&pasting

;;; Macro ANNOTATE-DOCUMENTATION:
;;;
;;; This macro is purposely small and simple, so that users can
;;; copy&paste it easily.
;;;
;;; The intention is to allow annotations in user code, without requiring
;;; a compilation-time dependency of the user's system to ours.
;;;
;;; We should try to guarantee API stability and minimize changes to this
;;; function so that copy&pasted definitions of the macro don't have to
;;; change.
;;;
;;; The macro has been written using keywords, so that it can be put into
;;; any package.
;;;
;;; Known issue:
;;;   For simplicity we store data in a plist of the symbol.
;;;   This restricts us to objects that have a symbol as their name and that
;;;   can be distinguished by the pair of name and doc-type.
;;;
;;;   This works well for variables, named classes, and named functions, at
;;;   least to the extent that our workaround for SETF functions is sufficient.
;;;
;;;   However, we can't annotate all objects permissible as a first argument to
;;;   the Common Lisp function :DOCUMENTATION using this method.
;;;   In particular, users must annotate function names, not function objects.
;;;
;;; A possible solution in the future would be to store annotations in
;;; a global hash table using weak references.  Since an approach based on
;;; weak-references would not be attractive as a small code snippet for
;;; copy&paste, we could offer two versions of this macro: The simple
;;; version below, and an exported fully-featured version in our system.
;;; That way only users with exotic annotation requirements would have to
;;; depend on our system.

;;; [ Start of code for copy&paste ]

(defmacro annotate-documentation
    ((name doc-type &key (key :documentation-annotations)) &body body)
  (if (and (consp name) (eq (car name) 'setf))
      `(annotate-documentation
	   (,(cadr name) ,doc-type :setf-documentation-annotations)
	 ,@body)
      `(setf (getf (get ',name ,key) ',doc-type) ',body)))

;;; [ End of code for copy&paste ]


;;; Example:

(annotate-documentation (annotate-documentation function)
  (:argument name
	     "The name of a variable, function, or type."
	     "Either a symbol or a list of the form (setf SYMBOL).")
  (:argument doc-type
	     "A symbol with the same meaning as the second argument to"
	     "Common Lisp's DOCUMENTATION function.")
  (:argument body
	     "Lists of the form (key [string or symbol]*)")
  "Records annotations for docstring.  This macro allows function arguments
   and return values to be documented separately from the function's docstring
   itself.  It also provides for cross-references between documentation
   strings without a textual reference in the docstring.

   Permissible forms in the body are:
     (:argument NAME DOCSTRING)
       Stores DOCSTRING as the description for argument NAME.

     (:return-value DOCSTRING)
     (:return-value NAME DOCSTRING)
       Stores DOCSTRING as the description for a return value.

     (:condition NAME)
       Documents that this function signals conditions of type NAME.

     (:constructor NAME)
       Documents that fresh instances of this class are returned by the
       function NAME.

     (:slot-accessor NAME)
       Documents that this class has a slot accessible through the function
       NAME, and possibly settable through (setf name).

       Most useful for programs that opt not to document slots, and prefer
       to document the slot accessors as ordinary functions instead.

     (:see-also-function NAME)
       Adds a simple cross reference to a related function NAME.

     (:see-also-variable NAME)
       Adds a simple cross reference to a related variable NAME.

     (:see-also-type NAME)
       Adds a simple cross reference to a related type NAME.")
