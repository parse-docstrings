;;; -*- lisp; show-trailing-whitespace: t; indent-tabs: nil -*-

;;;; Part of this software was originally written as docstrings.lisp in
;;;; SBCL, but is now part of the parse-docstrings project.  The file
;;;; docstrings.lisp was written by Rudi Schlatte <rudi@constantly.at>,
;;;; mangled by Nikodemus Siivola, turned into a stand-alone project by
;;;; Luis Oliveira.  SBCL is in the public domain and is provided with
;;;; absolutely no warranty.

;;;; parse-docstrings is:
;;;;
;;;; Copyright (c) 2008 David Lichteblau:
;;;;
;;;; Permission is hereby granted, free of charge, to any person
;;;; obtaining a copy of this software and associated documentation
;;;; files (the "Software"), to deal in the Software without
;;;; restriction, including without limitation the rights to use, copy,
;;;; modify, merge, publish, distribute, sublicense, and/or sell copies
;;;; of the Software, and to permit persons to whom the Software is
;;;; furnished to do so, subject to the following conditions:
;;;;
;;;; The above copyright notice and this permission notice shall be
;;;; included in all copies or substantial portions of the Software.
;;;;
;;;; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;; EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;; MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
;;;; NONINFRINGEMENT.  IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
;;;; HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
;;;; WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
;;;; OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
;;;; DEALINGS IN THE SOFTWARE.

#+sbcl ;; FIXME: should handle this in the .asd file
(eval-when (:compile-toplevel :load-toplevel :execute)
  (require 'sb-introspect))

(in-package #:parse-docstrings)

(defun function-arglist (function)
  #+sbcl (sb-introspect:function-arglist function)
  #-sbcl (error "function-arglist unimplemented"))

;;;; various specials and parameters

(defvar *documentation-package*)
(defvar *documentation-package-name*)

(defparameter *documentation-types*
  '(function
    method-combination
    setf
    ;;structure  ; also handled by `type'
    type
    variable)
  "A list of symbols accepted as second argument of `documentation'")

(defparameter *ordered-documentation-kinds*
  '(package type structure condition class macro))

;;;; utilities

(defun flatten (list)
  (cond ((null list)
         nil)
        ((consp (car list))
         (nconc (flatten (car list)) (flatten (cdr list))))
        ((null (cdr list))
         (cons (car list) nil))
        (t
         (cons (car list) (flatten (cdr list))))))

(defun flatten-to-string (list)
  (format nil "~{~A~^~%~}" (flatten list)))

(defun setf-name-p (name)
  (or (symbolp name)
      (and (listp name) (= 2 (length name)) (eq (car name) 'setf))))

(defgeneric specializer-name (specializer))

(defmethod specializer-name ((specializer eql-specializer))
  (list 'eql (eql-specializer-object specializer)))

(defmethod specializer-name ((specializer class))
  (class-name specializer))

(defun specialized-lambda-list (method)
  ;; courtecy of AMOP p. 61
  (let* ((specializers (method-specializers method))
         (lambda-list (method-lambda-list method))
         (n-required (length specializers)))
    (append (mapcar (lambda (arg specializer)
                      (if  (eq specializer (find-class 't))
                           arg
                           `(,arg ,(specializer-name specializer))))
                    (subseq lambda-list 0 n-required)
                    specializers)
           (subseq lambda-list n-required))))

(defun docstring (x doc-type)
  (handler-bind ((warning #'muffle-warning))
    (cl:documentation x doc-type)))



;;;; generating various names

(defgeneric name (thing)
  (:documentation "Name for a documented thing. Names are either
symbols or lists of symbols."))

(defmethod name ((symbol symbol))
  symbol)

(defmethod name ((cons cons))
  cons)

(defmethod name ((package package))
  (package-name package))

(defmethod name ((method method))
  (list
   (generic-function-name (method-generic-function method))
   (method-qualifiers method)
   (specialized-lambda-list method)))


;;;; finding a package

(defmethod thing-to-package ((symbol symbol))
  (symbol-package symbol))

(defmethod thing-to-package ((setf-name cons))
  (symbol-package (second setf-name)))

(defmethod thing-to-package ((package package))
  package)

(defmethod thing-to-package ((function function))
  (thing-to-package
   ;; fixme: use swank?
   #+sbcl (sb-impl::%fun-name function)))

(defmethod thing-to-package ((function generic-function))
  (thing-to-package (generic-function-name function)))

(defmethod thing-to-package ((method method))
  (thing-to-package (generic-function-name (method-generic-function method))))



;;;; frontend selection

(defvar *default-docstring-parser*
  'parse-docstrings.sbcl:parse-docstring)

(defgeneric docstring-parser-for (x))

(defmethod docstring-parser-for ((x package))
  (let ((configuration
	 (find-symbol "DOCSTRING-PARSER" x)))
    (if (and configuration (fboundp configuration))
	(let ((parser (funcall configuration)))
	  (unless (and parser
		       (typep parser '(and (not null) (or symbol cons)))
		       ;; fixme: fboundp errors
		       (fboundp parser))
	    (error "expected a parser function from ~A but got ~A"
		   configuration parser))
	  parser)
	*default-docstring-parser*)))

(defmethod docstring-parser-for ((x symbol))
  (docstring-parser-for (symbol-package x)))


;;;; methods related to the documentation* class

(defun get-symbol (doc)
  (let ((name (get-name doc)))
    (cond ((symbolp name)
           name)
          ((and (consp name) (eq 'setf (car name)))
           (second name))
          (t
           (error "Don't know which symbol to sort by: ~S" name)))))

(defmethod print-object ((documentation documentation*) stream)
  (print-unreadable-object (documentation stream :type t)
    (princ (list (get-kind documentation) (get-name documentation)) stream)))

(defgeneric make-documentation (x doc-type string))

(defmethod make-documentation :around (x doc-type string)
  (let ((doc (call-next-method)))
    (multiple-value-bind (markup annotations)
	(let ((*package* *documentation-package*))
	  (funcall (docstring-parser-for x) string))
      (setf (child-elements doc)
	    (append (child-elements doc) (simplify-markup markup)))
      (setf (documentation-annotations doc)
	    (merge-annotations (documentation-annotations doc)
			       annotations)))
    doc))

(defmethod make-documentation ((x package) doc-type string)
  (declare (ignore doc-type string))
  (make-instance 'documentation*
                 :name (name x)
                 :kind 'package))

(defmethod make-documentation (x (doc-type (eql 'function)) string)
  (declare (ignore doc-type string))
  (let* ((fdef (and (fboundp x) (fdefinition x)))
         (name x)
         (kind (cond ((and (symbolp x) (special-operator-p x))
                      'special-operator)
                     ((and (symbolp x) (macro-function x))
                      'macro)
                     ((typep fdef 'generic-function)
                      (assert (or (symbolp name) (setf-name-p name)))
                      'generic-function)
                     (fdef
                      (assert (or (symbolp name) (setf-name-p name)))
                      'function)))
         (methods (when (eq kind 'generic-function)
		    (collect-gf-documentation fdef))))
    (make-instance 'documentation*
                   :name (name x)
                   :kind kind
                   :methods methods)))

(defmethod make-documentation ((x method) doc-type string)
  (declare (ignore doc-type string))
  (make-instance 'documentation*
                 :name (name x)
                 :kind 'method))

(defmethod make-documentation (x (doc-type (eql 'type)) string)
  (declare (ignore string))
  (make-instance 'documentation*
                 :name (name x)
                 :kind (etypecase (find-class x nil)
                         (structure-class 'structure)
                         (standard-class 'class)
                         (sb-pcl::condition-class 'condition)
                         ((or built-in-class null) 'type))))

(defmethod make-documentation (x (doc-type (eql 'variable)) string)
  (declare (ignore string))
  (make-instance 'documentation*
                 :name (name x)
                 :kind (if (constantp x)
                           'constant
                           'variable)))

(defmethod make-documentation (x (doc-type (eql 'setf)) string)
  (declare (ignore doc-type string))
  (make-instance 'documentation*
                 :name (name x)
                 :kind 'setf-expander))

(defmethod make-documentation (x doc-type string)
  (declare (ignore string))
  (make-instance 'documentation*
                 :name (name x)
                 :kind doc-type))

(defun documentation* (x doc-type)
  "Returns a DOCUMENTATION instance for X and DOC-TYPE, or NIL if
there is no corresponding docstring."
  (let* ((*documentation-package* (thing-to-package x))
	 (*documentation-package-name* (package-name *documentation-package*))
	 (docstring (docstring x doc-type)))
    (when (plusp (length docstring))
      (make-documentation x doc-type docstring))))

(defun lambda-list (doc)
  ;; KLUDGE: Eugh.
  ;;
  ;; believe it or not, the above comment was written before CSR came along
  ;; and obfuscated this. (2005-07-04)
  (labels ((clean (x &key optional key)
             (typecase x
               (atom x)
               ((cons (member &optional))
                (cons (car x) (clean (cdr x) :optional t)))
               ((cons (member &key))
                (cons (car x) (clean (cdr x) :key t)))
               ;; &ENVIRONMENT ENV and &WHOLE FORM is not interesting to the user.
               ((cons (member &environment &whole))
                (clean (cddr x) :optional optional :key key))
               ((cons cons)
                (cons
                 (cond (key (if (consp (caar x))
                                (caaar x)
                                (caar x)))
                       (optional (caar x))
                       (t (clean (car x))))
                 (clean (cdr x) :key key :optional optional)))
               (cons
                (cons
                 (cond ((or key optional) (car x))
                       (t (clean (car x))))
                 (clean (cdr x) :key key :optional optional))))))
    (case (get-kind doc)
      ((package constant variable structure class condition nil)
       nil)
;;;       ((type)
;;;        ;; FIND-SYMBOL to avoid compilation errors on older SBCL versions:
;;;        (clean (funcall (find-symbol "INFO" :sb-int)
;;; 		       :type
;;; 		       :lambda-list
;;; 		       (get-name doc))))
      (method
       (third (get-name doc)))
      (t
       (when (symbolp (get-name doc))
	 (clean (function-arglist (get-name doc))))))))

(defun documentation< (x y)
  (let ((p1 (position (get-kind x) *ordered-documentation-kinds*))
        (p2 (position (get-kind y) *ordered-documentation-kinds*)))
    (cond ((and p1 p2 (/= p1 p2))
           (< p1 p2))
          ((and (or p1 p2) (not (and p1 p2)))
           (and p1 t))
          (t
           (string< (string (get-symbol x)) (string (get-symbol y)))))))


;;;; markup tree beautification

(defun simplify-markup (nodes)
  (cond
    ((endp nodes)
     nil)
    ((typep (car nodes) 'text)
     (let ((str
	    (with-output-to-string (s)
	      (iter (while (typep (car nodes) 'text))
		    (write-string (characters (car nodes)) s)
		    (pop nodes)))))
       (if (plusp (length str))
	   (cons (make-text str) (simplify-markup nodes))
	   (simplify-markup nodes))))
    (t
     (let ((car (car nodes)))
       (setf (child-elements car)
	     (simplify-markup (child-elements car))) 
       (cons car (simplify-markup (rest nodes)))))))


;;;; MISSING-SYMBOL support functions

(defun parse-symbol (str package)
  (handler-case
      (let ((*package* package))
	(read-from-string str))
    (error ()
      (parse-missing-symbol str package))))

;;; KLUDGE, can we do better?
(defun parse-missing-symbol (str package)
  (let ((pos1 (position #\: str)))
    (if pos1
	(let ((pos2 (position #\: str :from-end t)))
	  (make-missing-symbol (subseq str 0 pos1)
			       (subseq str (1+ pos2))))
	(make-missing-symbol str package))))

;;; works for both missing and actual symbols
(defun look-up-missing-symbol (symbol doc-type)
  (hyperdoc:lookup (missing-symbol-package-name symbol)
		   (missing-symbol-name symbol)
		   doc-type))


;;;; main logic

(defun collect-gf-documentation (gf)
  "Collects method documentation for the generic function GF"
  (iter (for method in (generic-function-methods gf))
        (for doc = (documentation* method t))
        (when doc
	  (collect doc))))

(defun collect-name-documentation (name)
  (iter (for type in *documentation-types*)
        (for doc = (documentation* name type))
        (when doc
	  (collect doc))))

(defun collect-symbol-documentation (symbol)
  "Collects all docs for a SYMBOL and (SETF SYMBOL), returns a list of
the form DOC instances. See `*documentation-types*' for the possible
values of doc-type."
  (nconc (collect-name-documentation symbol)
         (collect-name-documentation (list 'setf symbol))))

(defun collect-documentation (package &optional package-name)
  "Collects all documentation for all external symbols of the given
package, as well as for the package itself."
  (let* ((*documentation-package* (find-package package))
         (*documentation-package-name*
          (or package-name (package-name *documentation-package*)))
         (docs nil))
    (check-type package package)
    (do-external-symbols (symbol package)
      (setf docs (nconc (collect-symbol-documentation symbol) docs)))
    (let ((doc (documentation* *documentation-package* t)))
      (when doc
        (push doc docs)))
    docs))
