;;; -*- lisp; show-trailing-whitespace: t; indent-tabs: nil -*-

;;;; Part of this software was originally written as docstrings.lisp in
;;;; SBCL, but is now part of the parse-docstrings project.  The file
;;;; docstrings.lisp was written by Rudi Schlatte <rudi@constantly.at>,
;;;; mangled by Nikodemus Siivola, turned into a stand-alone project by
;;;; Luis Oliveira.  SBCL is in the public domain and is provided with
;;;; absolutely no warranty.

;;;; parse-docstrings is:
;;;;
;;;; Copyright (c) 2008 David Lichteblau:
;;;;
;;;; Permission is hereby granted, free of charge, to any person
;;;; obtaining a copy of this software and associated documentation
;;;; files (the "Software"), to deal in the Software without
;;;; restriction, including without limitation the rights to use, copy,
;;;; modify, merge, publish, distribute, sublicense, and/or sell copies
;;;; of the Software, and to permit persons to whom the Software is
;;;; furnished to do so, subject to the following conditions:
;;;;
;;;; The above copyright notice and this permission notice shall be
;;;; included in all copies or substantial portions of the Software.
;;;;
;;;; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;; EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;; MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
;;;; NONINFRINGEMENT.  IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
;;;; HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
;;;; WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
;;;; OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
;;;; DEALINGS IN THE SOFTWARE.

(defpackage #:parse-docstrings
  (:use #:cl #:c2mop :iterate)
  (:export #:documentation*
	   #:collect-documentation
	   #:collect-symbol-documentation
	   #:documentation<
	   #:setf-name-p
	   #:lambda-list)

  (:export #:markup-element
	   #:block-content
	   #:inline-content
	   #:parent-mixin
	   #:text
	   #:preformatted
	   #:code-block
	   #:inline-code
	   #:itemization
	   #:enumeration
	   #:paragraph
	   #:div
	   #:span
	   #:bold
	   #:italic
	   #:fixed-width
	   #:underline
	   #:definition-list
	   #:definition-list-item
	   #:hyperlink
	   #:inline-cross-reference
	   #:unknown-element

	   #:characters
	   #:child-elements
	   #:list-items
	   #:definition-title

	   #:make-text
	   #:make-preformatted
	   #:make-fixed-width
	   #:make-code-block
	   #:make-inline-code
	   #:make-itemization
	   #:make-enumeration
	   #:make-paragraph
	   #:make-div
	   #:make-span
	   #:make-bold
	   #:make-italic
	   #:make-underline
	   #:make-list-item
	   #:make-definition-list
	   #:make-definition-list-item
	   #:make-hyperlink
	   #:make-unknown-element
	   #:make-inline-cross-reference)

  (:export #:documentation-annotations
	   #:cross-reference-annotation
	   #:parameter-annotation
	   #:summary-annotation
	   #:implementation-note-annotation

	   #:argument-annotations
	   #:return-value-annotations
	   #:condition-annotations
	   #:slot-accessor-annotations
	   #:constructor-annotations
	   #:see-also-annotations
	   #:cross-reference-target
	   #:cross-reference-doc-type

	   #:make-parameter-annotation
	   #:make-cross-reference-annotation

	   #:sort-annotations!)

  (:export #:missing-symbol
	   #:missing-symbol-name
	   #:missing-symbol-package-name
	   #:parse-symbol
	   #:look-up-missing-symbol)

  (:export #:markup-to-sexp
	   #:sexp-to-markup))

(defpackage #:parse-docstrings.sbcl
  (:use :cl)
  (:export #:parse-docstring))

(defpackage #:parse-docstrings.atsign
  (:use :cl)
  (:export #:parse-docstring))
