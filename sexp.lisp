;;; -*- lisp; show-trailing-whitespace: t; indent-tabs: nil -*-

;;;; Copyright (c) 2008 David Lichteblau:
;;;;
;;;; Permission is hereby granted, free of charge, to any person
;;;; obtaining a copy of this software and associated documentation
;;;; files (the "Software"), to deal in the Software without
;;;; restriction, including without limitation the rights to use, copy,
;;;; modify, merge, publish, distribute, sublicense, and/or sell copies
;;;; of the Software, and to permit persons to whom the Software is
;;;; furnished to do so, subject to the following conditions:
;;;;
;;;; The above copyright notice and this permission notice shall be
;;;; included in all copies or substantial portions of the Software.
;;;;
;;;; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;; EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;; MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
;;;; NONINFRINGEMENT.  IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
;;;; HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
;;;; WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
;;;; OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
;;;; DEALINGS IN THE SOFTWARE.

(in-package #:parse-docstrings)


;;; CLOS to Sexp

(defun children-to-sexp (x)
  (mapcar #'markup-to-sexp (child-elements x)))

(defgeneric markup-to-sexp (markup))

(defmethod markup-to-sexp ((x text))
  (characters x))

(defmethod markup-to-sexp ((x documentation*))
  `(:documentation ,@(children-to-sexp x)))

(defmethod markup-to-sexp ((x preformatted))
  `(:pre ,@(children-to-sexp x)))

(defmethod markup-to-sexp ((x code-block))
  `(:code-block ,@(children-to-sexp x)))

(defmethod markup-to-sexp ((x itemization))
  `(:ul ,@(children-to-sexp x)))

(defmethod markup-to-sexp ((x enumeration))
  `(:ol ,@(children-to-sexp x)))

(defmethod markup-to-sexp ((x paragraph))
  `(:p ,@(children-to-sexp x)))

(defmethod markup-to-sexp ((x div))
  `(:div ,@(children-to-sexp x)))

(defmethod markup-to-sexp ((x span))
  `(:span ,@(children-to-sexp x)))

(defmethod markup-to-sexp ((x bold))
  `(:b ,@(children-to-sexp x)))

(defmethod markup-to-sexp ((x italic))
  `(:i ,@(children-to-sexp x)))

(defmethod markup-to-sexp ((x fixed-width))
  `(:tt ,@(children-to-sexp x)))

(defmethod markup-to-sexp ((x inline-code))
  `(:inline-code ,@(children-to-sexp x)))

(defmethod markup-to-sexp ((x underline))
  `(:u ,@(children-to-sexp x)))

(defmethod markup-to-sexp ((x definition-list))
  `(:dl ,@(iter (for item in (list-items x))
		(collect `(:dt ,(definition-title item)))
		(collect `(:dd ,@(children-to-sexp item))))))

(defmethod markup-to-sexp ((x hyperlink))
  `(:a (:href ,(href x))
       ,@(children-to-sexp x)))

(defmethod markup-to-sexp ((x inline-cross-reference))
  `(:xref (:target ,(cross-reference-target x)
		   :doc-type ,(cross-reference-doc-type x)
		   :annotation-category ,(annotation-category x))
	  ,@(children-to-sexp x)))

(defmethod unknown-element ((x hyperlink))
  `(,(name x)
     ,(plist x)
     ,@(children-to-sexp x)))


;;; Sexp to CLOS

(defun sexp-to-markup (x)
  (typecase x
    (string (make-text x))
    (cons (sexp-to-markup-using-car (car x) x))
    (null nil)))

(defun body-to-markup (x)
  (mapcar #'sexp-to-markup x))

(defmethod sexp-to-markup-using-car ((car (eql :pre)) x)
  (apply #'make-preformatted (body-to-markup (cdr x))))

(defmethod sexp-to-markup-using-car ((car (eql :code-block)) x)
  (apply #'make-code-block (body-to-markup (cdr x))))

(defmethod sexp-to-markup-using-car ((car (eql :inline-code)) x)
  (apply #'make-inline-code (body-to-markup (cdr x))))

(defmethod sexp-to-markup-using-car ((car (eql :ul)) x)
  (apply #'make-itemization (body-to-markup (cdr x))))

(defmethod sexp-to-markup-using-car ((car (eql :ol)) x)
  (apply #'make-enumeration (body-to-markup (cdr x))))

(defmethod sexp-to-markup-using-car ((car (eql :p)) x)
  (apply #'make-paragraph (body-to-markup (cdr x))))

(defmethod sexp-to-markup-using-car ((car (eql :div)) x)
  (apply #'make-div (body-to-markup (cdr x))))

(defmethod sexp-to-markup-using-car ((car (eql :span)) x)
  (apply #'make-span (body-to-markup (cdr x))))

(defmethod sexp-to-markup-using-car ((car (eql :b)) x)
  (apply #'make-bold (body-to-markup (cdr x))))

(defmethod sexp-to-markup-using-car ((car (eql :i)) x)
  (apply #'make-italic (body-to-markup (cdr x))))

(defmethod sexp-to-markup-using-car ((car (eql :u)) x)
  (apply #'make-underline (body-to-markup (cdr x))))

(defmethod sexp-to-markup-using-car ((car (eql :dl)) x)
  (apply #'make-definition-list
	 (iter (for (dt dl) in (cdr x))
	       (check-type dt (cons (eql :dt) (cons string null)))
	       (check-type (car dl) (eql :dl))
	       (collect (make-definition-list-item (second dt)
						   (body-to-markup (cdr dl)))))))

(defmethod sexp-to-markup-using-car ((car (eql :xref)) x)
  (destructuring-bind ((&key target doc-type annotation-category) &body body)
      (cdr x)
    (apply #'make-inline-cross-reference
	   target
	   doc-type
	   annotation-category
	   (body-to-markup body))))
