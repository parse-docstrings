;;; -*- show-trailing-whitespace: t; indent-tabs: nil -*-

;;; Copyright (c) 2006,2007,2008,2009 David Lichteblau. All rights reserved.

;;; Redistribution and use in source and binary forms, with or without
;;; modification, are permitted provided that the following conditions
;;; are met:
;;;
;;;   * Redistributions of source code must retain the above copyright
;;;     notice, this list of conditions and the following disclaimer.
;;;
;;;   * Redistributions in binary form must reproduce the above
;;;     copyright notice, this list of conditions and the following
;;;     disclaimer in the documentation and/or other materials
;;;     provided with the distribution.
;;;
;;; THIS SOFTWARE IS PROVIDED BY THE AUTHOR 'AS IS' AND ANY EXPRESSED
;;; OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
;;; WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
;;; ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
;;; DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
;;; DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
;;; GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
;;; INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
;;; WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
;;; NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
;;; SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

(in-package #:parse-docstrings.atsign)

(defvar *annotations*)

(defun parse-docstring (str)
  (with-input-from-string (s str)
    (let ((handle (make-instance 'handle))
	  (*annotations*
	   (make-instance 'parse-docstrings:documentation-annotations)))
      (parse-docstring-1 s handle nil)
      (parse-docstrings:sort-annotations! *annotations*)
      (values
       (handle-contents handle)
       *annotations*))))


;;; list builder

(defclass handle ()
  ((tail :accessor tail)
   (head :accessor head)))

(defmethod initialize-instance :after ((instance handle) &key)
  (setf (tail instance)
	(setf (head instance)
	      (list nil))))

(defun handle-contents (handle)
  (cdr (head handle)))

(defmethod push-element (elt (handle handle))
  (setf (cdr (tail handle)) (list elt))
  (pop (tail handle))
  elt)

(defun emit (into thing &rest args)
  (etypecase thing
    (symbol
     (push-element (apply thing args) into))
    (string
     (push-element (parse-docstrings:make-text thing) into))
    (parse-docstrings:markup-element
     (push-element thing into))))


;;; list builder for PARENT-MIXIN

(defclass parent-handle (handle)
  ((parent :initarg :parent)))

(defun make-parent-handle (parent)
  (make-instance 'parent-handle :parent parent))

(defmethod push-element :after (elt (handle parent-handle))
  (declare (ignore elt))
  (with-slots (parent) handle
    (unless (parse-docstrings:child-elements parent)
      (setf (parse-docstrings:child-elements parent)
	    (handle-contents handle)))))


;;; list builder for LIST-MIXIN

(defclass list-handle ()
  ((list :initarg :list)))

(defun make-list-handle (list)
  (make-instance 'list-handle :list list))

(defmethod push-element :after (elt (handle list-handle))
  (declare (ignore elt))
  (with-slots (list) handle
    (unless (parse-docstrings:list-items list)
      (setf (parse-docstrings:list-items list)
	    (handle-contents handle)))))


;;; builder for string syntax

(defclass text-handle (handle)
  ((stream :initform (make-string-output-stream))))

(defun make-text-handle ()
  (make-instance 'text-handle))

(defun text-handle-contents (handle)
  (with-slots (stream) handle
    (get-output-stream-string stream)))

(defmethod push-element
    :before
    (elt (handle text-handle))
  (unless (typep elt 'parse-docstrings:text)
    (error "expected characters in ~A but found ~A" handle elt))
  (with-slots (stream) handle
    (write-string (parse-docstrings:characters elt) stream)))


;;; 

(defun characters (into str)
  (let ((lines (coerce (split-sequence:split-sequence #\newline str) 'vector))
	(ignore nil))
    (emit into (elt lines 0))
    (when (> (length lines) 1)
      (loop
	 for i from 1 below (1- (length lines))
	 for line = (elt lines i)
	 do
	 (cond
	   ((zerop (length (string-trim " " line)))
	    (unless ignore
	      (emit into 'parse-docstrings::break-paragraph-hack))
	    (setf ignore t))
	   (t
	    (emit into (string #\newline))
	    (emit into line)
	    (setf ignore nil))))
      (emit into (elt lines (1- (length lines)))))))

(defun parse-docstring-1 (stream into close)
  (let ((out (make-string-output-stream)))
    (loop for c = (read-char stream nil) do
	  (cond
	    ((null c)
	      (when close
		(error "unexpected end of documentation string"))
	      (return))
	    ((eql c #\@)
	      (cond
		((eql (peek-char nil stream nil) #\})
		  (write-char (read-char stream) out))
		((eql (peek-char nil stream nil) #\@)
		  (write-char c out))
		(t
		  (emit into (get-output-stream-string out))
		  (let ((name (read-delimited-string stream "[{ :")))
		    (when (equal name "end")
		      (read-char stream)
		      (unless
			  (equal (read-delimited-string stream "}" t) close)
			(error "invalid close tag"))
		      (return))
		    (parse-docstring-element stream into name)))))
	    ((eql c #\})
	      (when (eq close t)
		(return))
	      (error "unexpected closing brace"))
	    (t
	      (write-char c out))))
    (emit into (get-output-stream-string out))))

(defun read-delimited-string (stream bag &optional eat-limit)
  (let ((out (make-string-output-stream)))
    (loop
	for c = (read-char stream nil)
	do
	  (when (null c)
	    (error "unexpected end of documentation string"))
	  (when (find c bag)
	    (unless eat-limit
	      (unread-char c stream))
	    (return (get-output-stream-string out)))
	  (write-char c out))))

(defparameter *element-parsers* (make-hash-table :test 'equal))

(defun parse-docstring-element (stream into name)
  (let ((close t)
	(arg nil)
	(first-char (read-char stream)))
    (when (eql first-char #\[)
      (setf arg (read-delimited-string stream "]" t))
      (setf first-char (read-char stream)))
    (case first-char
      (#\{)
;;;       (#\space)
;;;       (#\: )
      (t (error "expected opening brace, space, or colon")))
    (when (equal name "begin")
      (setf name (read-delimited-string stream "}" t))
      (setf close name))
    (let ((parser (gethash name *element-parsers*)))
      (flet ((continuation (new-into)
	       (parse-docstring-1 stream new-into close)))
	(if parser
	    (funcall parser into arg #'continuation)
	    (let* ((plist (list name name))
		   (elt (parse-docstrings:make-unknown-element name plist)))
	      (continuation (make-parent-handle elt))
	      (push-element elt into)))))))

(defmacro define-parser (name (into-var arg-var) &body body)
  (let ((symhack (make-symbol name)))
    `(setf (gethash ,name *element-parsers*)
	   (flet ((,symhack (,into-var ,arg-var continuation)
		    ;; fixme: should permit declarations instead of
		    ;; declaring ignorable unconditionally, but would want
		    ;; to use parse-declarations for that.
		    (declare (ignorable ,into-var ,arg-var))
		    (flet ((continue-parsing (into)
			     (funcall continuation into)))
		      ,@body)))
	     #',symhack))))

(define-parser "itemize" (into arg)
  (let ((list (parse-docstrings:make-itemization)))
    (continue-parsing (make-list-handle list))
    (push-element list into)))

(define-parser "enumerate" (into arg)
  (let ((list (parse-docstrings:make-enumeration)))
    (continue-parsing (make-list-handle list))
    (push-element list into)))

(define-parser "item" (into arg)
  (let ((item (parse-docstrings:make-list-item)))
    (continue-parsing (make-parent-handle item))
    (push-element item into)))

(define-parser "arg" (into arg)
  (let ((annotation (parse-docstrings:make-parameter-annotation arg)))
    (continue-parsing (make-parent-handle annotation))
    (push annotation
	  (parse-docstrings:argument-annotations *annotations*))))

(define-parser "return" (into arg)
  (let ((annotation (parse-docstrings:make-parameter-annotation arg)))
    (continue-parsing (make-parent-handle annotation))
    (push annotation
	  (parse-docstrings:return-value-annotations *annotations*))))

(define-parser "short" (into arg)
  (let ((annotation (parse-docstrings:make-div)))
    ;; record children as an annotation:
    (continue-parsing (make-parent-handle annotation))
    (setf (parse-docstrings:summary-annotation *annotations*)
	  annotation)
    ;; but also copy them into the main documentation, as if @short hadn't
    ;; been there:
    (dolist (child (parse-docstrings:child-elements annotation))
      (emit into child))))

(define-parser "implementation-note" (into arg)
  (let ((annotation (parse-docstrings:make-div)))
    ;; record children as an annotation:
    (continue-parsing (make-parent-handle annotation))
    (setf (parse-docstrings:implementation-note-annotation
	   *annotations*)
	  annotation)))

(macrolet ((%simple-parser (name fun)
	     `(define-parser ,name (into arg)
		(let ((parent (,fun)))
		  (continue-parsing (make-parent-handle parent))
		  (push-element parent into)))))
  (%simple-parser "b" parse-docstrings:make-bold)
  (%simple-parser "em" parse-docstrings:make-italic)
  (%simple-parser "u" parse-docstrings:make-underline)
  (%simple-parser "tt" parse-docstrings:make-fixed-width)
  (%simple-parser "code" parse-docstrings:make-inline-code)
  (%simple-parser "var" parse-docstrings:make-inline-code)
  (%simple-parser "pre" parse-docstrings:make-preformatted))

(define-parser "a" (into arg)
  (let ((item (parse-docstrings:make-hyperlink arg)))
    (continue-parsing (make-parent-handle item))
    (push-element item into)))

;;; "aboutfun"
;;; "aboutmacro"
;;; "aboutclass"
;;; "sections"
;;; "section"

;;; (munged-name
;;; 			(handler-case
;;; 			    (munge-name
;;; 			     (let ((*package* *current-package*))
;;; 			       (read-from-string text))
;;; 			     (current-kind handler))
;;; 			  (error (c)
;;; 			    (warn "ignoring ~A" c)
;;; 			    nil)))

(macrolet ((%inline-xref (name doctype annotation-category)
	     `(define-parser ,name (into arg)
		(let* ((handle (make-text-handle))
		       (text (progn (continue-parsing handle)
				    (text-handle-contents handle)))
		       (sym (parse-docstrings:parse-symbol
			     text
			     *package*))
		       (xref
			(parse-docstrings:make-inline-cross-reference
			 sym ',doctype ',annotation-category))
		       (xref-handle (make-parent-handle xref)))
		  (dolist (child (handle-contents handle))
		    (push-element child xref-handle))
		  (push-element xref into)))))
  (%inline-xref "fun" function nil)
  (%inline-xref "class" type nil)
  (%inline-xref "type" type nil)
  (%inline-xref "variable" variable nil)
  (%inline-xref "slot" function :slot-accessor)
  (%inline-xref "condition" type :condition)
  (%inline-xref "constructor" function :constructor))

(macrolet ((%xref-annotation (name doctype slot)
	     `(define-parser ,name (into arg)
		(let* ((handle (make-text-handle))
		       (text (progn (continue-parsing handle)
				    (text-handle-contents handle))))
		  (push (parse-docstrings:make-cross-reference-annotation
			 (parse-docstrings:parse-symbol text *package*)
			 ',doctype)
			(,slot *annotations*))))))
  (%xref-annotation "see"
		    function
		    parse-docstrings:see-also-annotations)
  (%xref-annotation "see-class"
		    type
		    parse-docstrings:see-also-annotations)
  (%xref-annotation "see-type"
		    type
		    parse-docstrings:see-also-annotations)
  (%xref-annotation "see-variable"
		    variable
		    parse-docstrings:see-also-annotations)
  (%xref-annotation "see-slot"
		    function
		    parse-docstrings:slot-accessor-annotations)
  (%xref-annotation "see-constructor"
		    function
		    parse-docstrings:constructor-annotations))
