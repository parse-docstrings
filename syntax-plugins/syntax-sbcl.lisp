;;; -*- lisp; show-trailing-whitespace: t; indent-tabs: nil -*-

;;;; Part of this software was originally written as docstrings.lisp in
;;;; SBCL, but is now part of the parse-docstrings project.  The file
;;;; docstrings.lisp was written by Rudi Schlatte <rudi@constantly.at>,
;;;; mangled by Nikodemus Siivola, turned into a stand-alone project by
;;;; Luis Oliveira.  SBCL is in the public domain and is provided with
;;;; absolutely no warranty.

;;;; parse-docstrings is:
;;;;
;;;; Copyright (c) 2008 David Lichteblau:
;;;;
;;;; Permission is hereby granted, free of charge, to any person
;;;; obtaining a copy of this software and associated documentation
;;;; files (the "Software"), to deal in the Software without
;;;; restriction, including without limitation the rights to use, copy,
;;;; modify, merge, publish, distribute, sublicense, and/or sell copies
;;;; of the Software, and to permit persons to whom the Software is
;;;; furnished to do so, subject to the following conditions:
;;;;
;;;; The above copyright notice and this permission notice shall be
;;;; included in all copies or substantial portions of the Software.
;;;;
;;;; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;; EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;; MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
;;;; NONINFRINGEMENT.  IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
;;;; HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
;;;; WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
;;;; OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
;;;; DEALINGS IN THE SOFTWARE.

;;;; TODO
;;;; * Verbatim text
;;;; * Quotations
;;;; * This is getting complicated enough that tests would be good
;;;; * Nesting (currently only nested itemizations work)

;;;; Formatting heuristics (tweaked to format SAVE-LISP-AND-DIE sanely):
;;;;
;;;; Formats SYMBOL as @code{symbol}, or @var{symbol} if symbol is in
;;;; the argument list of the defun / defmacro.
;;;;
;;;; Lines starting with * or - that are followed by intented lines
;;;; are marked up with @itemize.
;;;;
;;;; Lines containing only a SYMBOL that are followed by indented
;;;; lines are marked up as @table @code, with the SYMBOL as the item.

(in-package #:parse-docstrings.sbcl)


;;;; utilities

(defun string-lines (string)
  "Lines in STRING as a vector."
  (coerce (with-input-from-string (s string)
            (loop for line = (read-line s nil nil)
               while line collect line))
          'vector))

(defun whitespacep (char)
  (find char #(#\tab #\space #\page)))

(defun indentation (line)
  "Position of first non-SPACE character in LINE."
  (position-if-not (lambda (c) (char= c #\Space)) line))

(defun flatten (list)
  (cond ((null list)
         nil)
        ((consp (car list))
         (nconc (flatten (car list)) (flatten (cdr list))))
        ((null (cdr list))
         (cons (car list) nil))
        (t
         (cons (car list) (flatten (cdr list))))))

(defun flatten-to-string (list)
  (format nil "~{~A~^~%~}" (flatten list)))


;;; line markups

(defparameter *itemize-start-characters* '(#\* #\-)
  "Characters that might start an itemization in docstrings when
  at the start of a line.")

(defparameter *symbol-characters* "ABCDEFGHIJKLMNOPQRSTUVWXYZ*:-+&"
  "List of characters that make up symbols in a docstring.")

(defparameter *symbol-delimiters* " ,.!?;
")

(defun locate-symbols (line)
  "Return a list of index pairs of symbol-like parts of LINE."
  ;; This would be a good application for a regex ...
  (do ((result nil)
       (begin nil)
       (maybe-begin t)
       (i 0 (1+ i)))
      ((= i (length line))
       ;; symbol at end of line
       (when (and begin (or (> i (1+ begin))
                            (not (member (char line begin) '(#\A #\I)))))
         (push (list begin i) result))
       (nreverse result))
    (cond
      ((and begin (find (char line i) *symbol-delimiters*))
       ;; symbol end; remember it if it's not "A" or "I"
       (when (or (> i (1+ begin)) (not (member (char line begin) '(#\A #\I))))
         (push (list begin i) result))
       (setf begin nil
             maybe-begin t))
      ((and begin (not (find (char line i) *symbol-characters*)))
       ;; Not a symbol: abort
       (setf begin nil))
      ((and maybe-begin (not begin) (find (char line i) *symbol-characters*))
       ;; potential symbol begin at this position
       (setf begin i
             maybe-begin nil))
      ((find (char line i) *symbol-delimiters*)
       ;; potential symbol begin after this position
       (setf maybe-begin t))
      (t
       ;; Not reading a symbol, not at potential start of symbol
       (setf maybe-begin nil)))))

;;; lisp sections

(defun lisp-section-p (line line-number lines)
  "Returns T if the given LINE looks like start of lisp code --
ie. if it starts with whitespace followed by a paren or
semicolon, and the previous line is empty"
  (let ((offset (indentation line)))
    (and offset
         (plusp offset)
         (find (find-if-not #'whitespacep line) "(;")
         (empty-p (1- line-number) lines))))

(defun collect-lisp-section (lines line-number)
  (let ((lisp (loop for index = line-number then (1+ index)
                    for line = (and (< index (length lines))
                                    (svref lines index))
                    while (indentation line)
                    collect line)))
    (values (length lisp) `("@lisp" ,@lisp "@end lisp"))))

;;; itemized sections

(defun maybe-itemize-offset (line)
  "Return NIL or the indentation offset if LINE looks like it starts
an item in an itemization."
  (let* ((offset (indentation line))
         (char (when offset (char line offset))))
    (and offset
         (member char *itemize-start-characters* :test #'char=)
         (char= #\Space (find-if-not (lambda (c) (char= c char))
                                     line :start offset))
         offset)))

;;; is this bitrot?  Can we remove it?
#+(or)
(defun collect-maybe-itemized-section (lines starting-line)
  ;; Return index of next line to be processed outside
  (let ((this-offset (maybe-itemize-offset (svref lines starting-line)))
        (result nil)
        (lines-consumed 0))
    (loop for line-number from starting-line below (length lines)
       for line = (svref lines line-number)
       for indentation = (indentation line)
       for offset = (maybe-itemize-offset line)
       do (cond
            ((not indentation)
             ;; empty line -- inserts paragraph.
             (push "" result)
             (incf lines-consumed))
            ((and offset (> indentation this-offset))
             ;; nested itemization -- handle recursively
             ;; FIXME: tabulations in itemizations go wrong
             (multiple-value-bind (sub-lines-consumed sub-itemization)
                 (collect-maybe-itemized-section lines line-number)
               (when sub-lines-consumed
                 (incf line-number (1- sub-lines-consumed)) ; +1 on next loop
                 (incf lines-consumed sub-lines-consumed)
                 (setf result (nconc (nreverse sub-itemization) result)))))
            ((and offset (= indentation this-offset))
             ;; start of new item
             (push (format nil "@item ~A"
                           (texinfo-line (subseq line (1+ offset))))
                   result)
             (incf lines-consumed))
            ((and (not offset) (> indentation this-offset))
             ;; continued item from previous line
             (push (texinfo-line line) result)
             (incf lines-consumed))
            (t
             ;; end of itemization
             (loop-finish))))
    ;; a single-line itemization isn't.
    (if (> (count-if (lambda (line) (> (length line) 0)) result) 1)
        (values lines-consumed `("@itemize" ,@(reverse result) "@end itemize"))
        nil)))

;;; tabulation sections

(defun tabulation-body-p (offset line-number lines)
  (when (< -1 line-number (length lines))
    (let ((offset2 (indentation (svref lines line-number))))
      (and offset2 (< offset offset2)))))

(defun tabulation-p (offset line-number lines direction)
  (let ((step  (ecase direction
                 (:backwards (1- line-number))
                 (:forwards (1+ line-number)))))
    (when (and (plusp line-number) (< line-number (length lines)))
      (and (eql offset (indentation (svref lines line-number)))
           (or (when (eq direction :backwards)
                 (empty-p step lines))
               (tabulation-p offset step lines direction)
               (tabulation-body-p offset step lines))))))

(defun empty-p (line-number lines)
  (or (eql -1 line-number)
      (and (< line-number (length lines))
           (not (indentation (svref lines line-number))))))

(defun maybe-tabulation-offset (line-number lines)
  "Return NIL or the indentation offset if LINE looks like it starts
an item in a tabulation. Ie, if it is (1) indented, (2) preceded by an
empty line, another tabulation label, or a tabulation body, (3) and
followed another tabulation label or a tabulation body."
  (let* ((line (svref lines line-number))
         (offset (indentation line))
         (prev (1- line-number))
         (next (1+ line-number)))
    (when (and offset (plusp offset))
      (and (or (empty-p prev lines)
               (tabulation-body-p offset prev lines)
               (tabulation-p offset prev lines :backwards))
           (or (tabulation-body-p offset next lines)
               (tabulation-p offset next lines :forwards))
           offset))))

;;; section markup

(defmacro with-maybe-section (index &rest forms)
  `(multiple-value-bind (count collected) (progn ,@forms)
    (when count
      (dolist (line collected)
        (write-line line *document-output*))
      (incf ,index (1- count)))))

(defmacro maybe-section (index &body forms)
  `(multiple-value-bind (count section) (progn ,@forms)
    (when count
      (push section parsed)
      (incf ,index (1- count)))))

(defun parse-docstring (docstring into)
  (let ((lines (string-lines docstring))
        (parsed nil)
        (current nil))
    (labels ((end-paragraph ()
               (when current
                 (push (parse-docstrings:make-paragraph
			(parse-docstrings:make-text
			 (flatten-to-string (reverse current))) )
                       parsed)
                 (setf current nil)))
             (add-line (line)
               (let ((trimmed (string-trim '(#\space #\tab) line)))
                 (if (plusp (length trimmed))
                     (push trimmed current)
                     (end-paragraph)))))
      (loop for line-number from 0 below (length lines)
           for line = (svref lines line-number)
           do (cond
                ((maybe-section line-number
                   (and (lisp-section-p line line-number lines)
                        (parse-lisp-block lines line-number))))
                ((maybe-section line-number
                   (and (maybe-itemize-offset line)
                        (parse-maybe-itemization lines line-number))))
                ((maybe-section line-number
                   (and (maybe-tabulation-offset line-number lines)
                        (parse-maybe-tabulation lines line-number))))
                (t
                 (add-line line))))
      (end-paragraph)
      (unless parsed
	(error "empty parse? ~S" docstring))
      (setf (parse-docstrings:child-elements into)
	    (reverse parsed)))))

(defun parse-lisp-block (lines line-number)
  (let ((lisp (loop for index = line-number then (1+ index)
                    for line = (and (< index (length lines)) (svref lines index))
                    while (indentation line)
                    collect line)))
    (values (length lisp) (parse-docstrings:make-code-block
			   (parse-docstrings:make-text
			    (flatten-to-string lisp))))))

(defun section (paragraphs)
  (if (cdr paragraphs)
      (apply #'parse-docstrings:make-paragraph paragraphs)
      (car paragraphs)))

(defun parse-maybe-itemization (lines starting-line)
  ;; Return index of next line to be processed outside
  (let ((this-offset (maybe-itemize-offset (svref lines starting-line)))
        (items nil)
        (paragraphs nil)
        (current nil)
        (lines-consumed 0))
    (labels ((end-paragraph ()
               (when current
                 (push (parse-docstrings:make-paragraph
			(parse-docstrings:make-text
			 (flatten-to-string (reverse current))))
                       paragraphs)
                 (setf current nil)))
             (end-item ()
               (end-paragraph)
               (when paragraphs
                 (push (section (reverse paragraphs)) items)
                 (setf paragraphs nil)))
             (add-line (line)
               (push (string-trim '(#\space #\tab) line) current)))
      (loop for line-number from starting-line below (length lines)
            for line = (svref lines line-number)
            for indentation = (indentation line)
            for offset = (maybe-itemize-offset line)
            do (cond
                 ((not indentation)
                  ;; empty line, a paragraph break
                  (end-paragraph)
                  (incf lines-consumed))
                 ((and offset (> indentation this-offset))
                  (end-item)
                  ;; nested itemization -- handle recursively FIXME:
                  ;; tabulations in itemizations go wrong
                  (multiple-value-bind (sub-lines-consumed sublist)
                      (parse-maybe-itemization lines line-number)
                    (when sub-lines-consumed
                      (incf line-number (1- sub-lines-consumed)) ; +1 on next loop
                      (incf lines-consumed sub-lines-consumed)
                      (push sublist items))))
                 ((and offset (= indentation this-offset))
                  ;; start of new item
                  (end-item)
                  (add-line (subseq line (1+ offset)))
                  (incf lines-consumed))
                 ((and (not offset) (> indentation this-offset))
                  ;; continued paragraph from previous line
                  (add-line line)
                  (incf lines-consumed))
                 (t
                  ;; end of itemization
                  (loop-finish))))
      (end-item))
    ;; a single-item itemization isn't.
    (if (> (length items) 1)
        (values lines-consumed
		(apply #'parse-docstrings:make-itemization
		       (nreverse items)))
        (values nil nil))))


(defun parse-maybe-tabulation (lines starting-line)
  ;; Return index of next line to be processed outside
  (let ((this-offset (maybe-tabulation-offset starting-line lines))
        (items nil)
        (paragraphs nil)
        (title nil)
        (current nil)
        (lines-consumed 0))
    (labels ((end-paragraph ()
               (when current
                 (push (parse-docstrings:make-paragraph
			(parse-docstrings:make-text
			 (flatten-to-string (reverse current))))
                       paragraphs)
                 (setf current nil)))
             (end-item ()
               (end-paragraph)
               (when paragraphs
                 (push (parse-docstrings:make-definition-list-item
			title
			(section (reverse paragraphs)))
                       items)
                 (setf paragraphs nil
                       title nil)))
             (set-title (line)
               (setf title (string-trim '(#\space #\tab) line)))
             (add-line (line)
               (push (string-trim '(#\space #\tab) line) current)))
      (assert this-offset)
      (loop for line-number from starting-line below (length lines)
            for line = (svref lines line-number)
            for indentation = (indentation line)
            for offset = (maybe-tabulation-offset line-number lines)
            do (cond
                 ((not indentation)
                  (end-paragraph)
                  (incf lines-consumed))
                 ((and offset (= indentation this-offset))
                  ;; start of new item, or continuation of previous item
                  (cond ((and title (not (maybe-tabulation-offset line-number lines)))
                         (add-line line))
                        (t
                         (end-item)
                         (set-title line)))
                  (incf lines-consumed))
                 ((> indentation this-offset)
                  ;; continued item from previous line
                  (add-line line)
                  (incf lines-consumed))
                 (t
                  ;; end of itemization
                  (loop-finish))))
      (end-item))
    (values lines-consumed
	    (apply #'parse-docstrings:make-definition-list (reverse items)))))
