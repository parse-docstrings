;;; -*- lisp; show-trailing-whitespace: t; indent-tabs: nil -*-

;;;; Copyright (c) 2008 David Lichteblau:
;;;;
;;;; Permission is hereby granted, free of charge, to any person
;;;; obtaining a copy of this software and associated documentation
;;;; files (the "Software"), to deal in the Software without
;;;; restriction, including without limitation the rights to use, copy,
;;;; modify, merge, publish, distribute, sublicense, and/or sell copies
;;;; of the Software, and to permit persons to whom the Software is
;;;; furnished to do so, subject to the following conditions:
;;;;
;;;; The above copyright notice and this permission notice shall be
;;;; included in all copies or substantial portions of the Software.
;;;;
;;;; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
;;;; EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
;;;; MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
;;;; NONINFRINGEMENT.  IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
;;;; HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
;;;; WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
;;;; OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
;;;; DEALINGS IN THE SOFTWARE.

(in-package :parse-docstrings)


;;;; Fetching annotations that were stored using ANNOTATION-DOCUMENTATION:

(defun raw-annotations (name doc-type)
  (let ((key :documentation-annotations))
    (when (and (consp name) (eq (car name) 'setf))
      (setf name (second name))
      (setf key :setf-documentation-annotations))
    (getf (get name key) doc-type)))

(defun destructure-raw-annotation (x)
  (let* ((kind (car x))
	 (sym nil)
	 (docstring
	  (with-output-to-string (s)
	    (dolist (y (cdr x))
	      (etypecase y
		(string
		 (fresh-line s)
		 (write-string y s))
		(symbol
		 (setf sym y))
		(t
		 (warn "ignoring unrecognized sub-annotation: ~A" y)))))))
    (values kind sym docstring)))

(defun parse-raw-annotations (annotations docstring-parser)
  (let ((d-a (make-instance 'documentation-annotations)))
    (with-slots (arguments
		 return-values
		 conditions
		 slot-accessors
		 constructors
		 see-also-list)
	d-a
      (let ((docstring
	     (with-output-to-string (s)
	       (dolist (x annotations)
		 (typecase x
		   (string
		    (fresh-line s)
		    (write-string x s))
		   (list
		    (multiple-value-bind (kind sym docstring)
			(destructure-raw-annotation x)
		      (ecase kind
			(:argument
			 (let ((annotation (make-parameter-annotation sym)))
			   (funcall docstring-parser docstring annotation)
			   (push annotation arguments)))
			(:return-value
			 (let ((annotation (make-parameter-annotation sym)))
			   (funcall docstring-parser docstring annotation)
			   (push annotation return-values)))
			(:condition
			 (push (make-instance 'cross-reference-annotation
					      :target sym
					      :doc-type 'type)
			       conditions))
			(:slot-accessor
			 (push (make-instance 'cross-reference-annotation
					      :target sym
					      :doc-type 'function)
			       slot-accessors))
			(:constructor
			 (push (make-instance 'cross-reference-annotation
					      :target sym
					      :doc-type 'function)
			       constructors))
			(:see-also-function
			 (push (make-instance 'cross-reference-annotation
					      :target sym
					      :doc-type 'function)
			       see-also-list))
			(:see-also-variable
			 (push (make-instance 'cross-reference-annotation
					      :target sym
					      :doc-type 'variable)
			       see-also-list))
			(:see-also-type
			 (push (make-instance 'cross-reference-annotation
					      :target sym
					      :doc-type 'type)
			       see-also-list)))))
		   (t
		    (warn "ignoring unrecognized annotation: ~A" x)))))))
	(sort-annotations! d-a)
	(values docstring d-a)))))

(defun pa< (p q)
  (string-lessp (parameter-name p) (parameter-name q)))

(defun xref< (x y)
  (cond
    ((missing-symbol< (cross-reference-target x) (cross-reference-target y))
     t)
    ((missing-symbol= (cross-reference-target x) (cross-reference-target y))
     nil)
    (t
     (string-lessp (cross-reference-doc-type x) (cross-reference-doc-type y)))))

(defun missing-symbol< (x y)
  (cond
    ((string-lessp (missing-symbol-name x) (missing-symbol-name y))
     t)
    ((string-equal (missing-symbol-name x) (missing-symbol-name y))
     nil)
    (t
     (string-lessp (missing-symbol-package-name x)
		   (missing-symbol-package-name y)))))

(defun missing-symbol= (x y)
  (and (string-equal (missing-symbol-name x)
		     (missing-symbol-name y))
       (string-equal (missing-symbol-package-name x)
		     (missing-symbol-package-name y))))

(defun sort-annotations! (d-a)
  (with-slots (arguments
	       return-values
	       conditions
	       slot-accessors
	       constructors
	       see-also-list)
      d-a
    (setf arguments (sort (copy-seq arguments) #'pa<))
    (setf return-values (sort (copy-seq return-values) #'pa<))
    (setf conditions (sort (copy-seq conditions) #'xref<))
    (setf slot-accessors (sort (copy-seq slot-accessors) #'xref<))
    (setf constructors (sort (copy-seq constructors) #'xref<))
    (setf see-also-list (sort (copy-seq see-also-list) #'xref<)))
  d-a)

(defun merge-annotations (a b)
  (if (and a b)
      (merge-annotations-2 a b)
      (or a b)))

(defun merge-annotations-2 (a b)
  (let ((c (make-instance 'documentation-annotations)))
    (macrolet ((%append (accessor)
		 `(setf (,accessor c)
			(append (,accessor a)
				(,accessor b)))))
      (%append argument-annotations)
      (%append return-value-annotations)
      (%append condition-annotations)
      (%append slot-accessor-annotations)
      (%append constructor-annotations)
      (%append see-also-annotations))
    ;; FIXME: should these be lists, too?
    (macrolet ((%override (accessor)
		 `(setf (,accessor c)
			(or (,accessor a)
			    (,accessor b)))))
      (%override summary-annotation)
      (%override implementation-note-annotation))
    (sort-annotations! c)))

(defun annotations (name doc-type)
  (parse-raw-annotations (raw-annotations name doc-type)
			 (docstring-parser-for name)))
